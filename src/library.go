package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strconv"
	"strings"
)

/*
 * broadcastToShardID == -1 => global broadcast
 * responseNeeded == false => return nil
 * Assume: Node requesting broadcast does not receive broadcast message back to itself
 */
func broadcastCustom(route string, typeReq string, form map[string]string, broadcastToShardID int, isResponseNeeded bool) (map[string]interface{}, int) {
	if form["broadcast"] == "" {
		form["broadcast"] = "true"
	} else {
		return nil, 0
	}

	var body string
	for key, val := range form {
		body += key + "=" + val + "&"
	}
	body = strings.TrimRight(body, "&")

	var init int
	var end int
	if broadcastToShardID == -1 {
		init = 0
		end = globalView.size()
	} else {
		init = broadcastToShardID
		end = broadcastToShardID + 1
	}
	var returnBody map[string]interface{}
	var statusCode int
	for shardID := init; shardID < end; shardID++ {
		for _, nodeIP := range globalView.getLocalView(shardID) {
			log.Println("Broadcast to", nodeIP)
			if nodeIP != "" {
				if nodeIP != myIPport() {
					url := "http://" + nodeIP + "/" + route
					client := http.Client{}

					request, err := http.NewRequest(typeReq, url, strings.NewReader(body))
					request.Header.Set("Content-Type", "application/x-www-form-urlencoded")
					if err != nil {
						log.Println(err)
					}
					resp, err := client.Do(request)
					if err != nil {
						log.Println(err)
					}
					returnBody = make(map[string]interface{})
					err = json.NewDecoder(resp.Body).Decode(&returnBody)
					if err != nil {
						log.Println(err)
					}
					defer resp.Body.Close()
					statusCode = resp.StatusCode
				}
			}
		}
	}
	return returnBody, statusCode
}

func parseForm(r *http.Request) map[string]string {
	body, _ := ioutil.ReadAll(r.Body)
	strBody, _ := url.QueryUnescape(string(body))
	form := make(map[string]string)
	if strBody != "" {
		for _, elm := range strings.Split(strBody, "&") {
			s := strings.Split(elm, "=")
			form[s[0]] = s[1]
		}
	}
	return form
}

func getID(ipport string) int {
	s := strings.Split(ipport, ":")
	s = strings.Split(s[0], ".")

	id, _ := strconv.Atoi(s[3])
	return id
}

func hashIP(ip string) int {
	return getID(ip) % globalView.getNumOfShards()
}

func hashString(key string) uint32 {
	stringToUnicode := []rune(key)

	var hash uint32
	const g uint32 = 41 // Good values 33, 37, 39, 41
	for _, value := range stringToUnicode {
		hash = g*hash + uint32(value)
	}
	return hash
}

func whichShard(key string) int {
	return int(hashString(key) % uint32(globalView.getNumOfShards()))
}

func sendRespondMsg(w http.ResponseWriter, statusCode int, msg *map[string]interface{}) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(statusCode)
	json.NewEncoder(w).Encode(msg)
}

func sendRequestMsg(route string, typeReq string, ipPort string, form map[string]string) {
	form["broadcast"] = "notRequired"
	var body string
	for key, val := range form {
		body += key + "=" + val + "&"
	}
	body = strings.TrimRight(body, "&")

	url := "http://" + ipPort + "/" + route
	client := http.Client{}

	request, err := http.NewRequest(typeReq, url, strings.NewReader(body))
	request.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	if err != nil {
		log.Println(err)
	}
	_, err = client.Do(request)
	if err != nil {
		log.Println(err)
	}

}
