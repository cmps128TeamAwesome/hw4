package main

import (
	"os"
	"strconv"
)

const globalBroadcast = -1
const responseRequired = true
const responseNotRequired = false

var k = newKvs()

var numOfShards, _ = strconv.Atoi(os.Getenv("S"))
var globalView = newGloballView(os.Getenv("VIEW"), numOfShards)
var iPPortPrivate = os.Getenv("IP_PORT")
