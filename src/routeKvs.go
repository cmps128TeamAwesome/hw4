package main

import (
	"encoding/json"
	"net/http"
	"time"

	"github.com/gorilla/mux"
)

func getKey(w http.ResponseWriter, r *http.Request) {
	key := mux.Vars(r)["key"]
	form := parseForm(r)
	var clientVectorClock map[string]int
	json.Unmarshal([]byte(form["payload"]), &clientVectorClock)

	if whichShard(key) == globalView.myShardID() {
		if k.isKeyExist(key) {
			sendRespondMsg(w, http.StatusOK, &map[string]interface{}{"result": "Success", "value": k.get(key), "owner": globalView.myShardID(), "payload": clientVectorClock})
		} else {
			sendRespondMsg(w, http.StatusNotFound, &map[string]interface{}{"result": "Error", "msg": "Key does not exist", "payload": form["payload"]})
		}
		// log.Println(k)
	} else {
		resp, statusCode := broadcastCustom("keyValue-store/"+key, "GET", form, whichShard(key), responseRequired)
		sendRespondMsg(w, statusCode, &resp)
	}
}

func putKey(w http.ResponseWriter, r *http.Request) {
	key := mux.Vars(r)["key"]
	form := parseForm(r)
	replaced := false
	msg := "Added successfully"
	statusCode := http.StatusCreated
	if k.isKeyExist(key) {
		replaced = true
		msg = "Updated successfully"
		statusCode = http.StatusOK
	}

	var t time.Time
	if form["timeStamp"] == "" {
		t = time.Now()
		form["timeStamp"] = t.String()
	} else {
		t, _ = time.Parse(time.Now().String(), form["timeStamp"])
	}
	if form["forcePut"] == "true" || globalView.myShardID() == whichShard(key) {
		k.put(key, form["val"], t)
		broadcastCustom("keyValue-store/"+key, "PUT", form, globalView.myShardID(), responseNotRequired)
	} else {
		broadcastCustom("keyValue-store/"+key, "PUT", form, whichShard(key), responseNotRequired)
	}
	sendRespondMsg(w, statusCode, &map[string]interface{}{"replaced": replaced, "msg": msg, "payload": myIPport()})
}

func deleteKey(w http.ResponseWriter, r *http.Request) {
	key := mux.Vars(r)["key"]
	form := parseForm(r)
	if whichShard(key) == globalView.myShardID() {
		if k.isKeyExist(key) {
			k.deleteKey(key)
			sendRespondMsg(w, http.StatusOK, &map[string]interface{}{"result": "Success", "msg": "Key deleted", "payload": form["payload"]})
		} else {
			sendRespondMsg(w, http.StatusNotFound, &map[string]interface{}{"result": "Error", "msg": "Key does not exist", "payload": form["payload"]})
		}
		broadcastCustom("keyValue-store/"+key, "DELETE", form, globalView.myShardID(), responseNotRequired)
	} else {
		resp, statusCode := broadcastCustom("keyValue-store/"+key, "DELETE", form, whichShard(key), responseRequired)
		sendRespondMsg(w, statusCode, &resp)
	}
}

func searchKey(w http.ResponseWriter, r *http.Request) {
	key := mux.Vars(r)["key"]
	form := parseForm(r)
	if whichShard(key) == globalView.myShardID() {
		if k.isKeyExist(key) {
			sendRespondMsg(w, http.StatusOK, &map[string]interface{}{"isExists": true, "result": "Success", "owner": globalView.myShardID(), "payload": parseForm(r)["payload"]})
		} else {
			sendRespondMsg(w, http.StatusOK, &map[string]interface{}{"isExists": false, "result": "Success", "payload": parseForm(r)["payload"]})
		}
	} else {
		resp, statusCode := broadcastCustom("keyValue-store/search/"+key, "GET", form, whichShard(key), responseRequired)
		sendRespondMsg(w, statusCode, &resp)
	}

}
