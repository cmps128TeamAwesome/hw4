# File structure and how to run / test things
### Notes about test script

- You must have `mynet` subnetted in `10.0.0.0/16`

- To Run test
```
python3 hw4_test.py
```
routeKvs.go
- implements PUT key method
- implements GET method
- implements DELETE key method
- implements SEARCH method (not used in this HW)

routeView.go
- implements GET method for VIEWS (/views)
- may add PUT and DELETE here 

routeShard.go
- implements GET shard id

kvs.go 
- implements getters and setters for kvs

library.go
- implements forwarding and broadcasting 

main.go 
- Initialize kvs
- initialize view

set.go 
- implements getters and setters for VIEWS (a set of views)

How to write new methods
- create a new file with the method name 
- make sure we're declaring `package main` so main.go will have access to this method






