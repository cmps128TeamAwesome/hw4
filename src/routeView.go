package main

import (
	"bytes"
	"encoding/json"
	"log"
	"net/http"
)

func getView(w http.ResponseWriter, r *http.Request) {
	sendRespondMsg(w, http.StatusOK, &map[string]interface{}{"view": globalView.toString(globalView.myShardID())})
}

func putView(w http.ResponseWriter, r *http.Request) {
	form := parseForm(r)
	ipport := form["ip_port"]

	if globalView.addNode(ipport) {
		if hashIP(ipport) == globalView.myShardID() {
			sendKVS(w, ipport)
		}
		log.Println(myIPport(), ": My changed view:", globalView.localView)
		sendRespondMsg(w, http.StatusOK, &map[string]interface{}{"result": "Success", "msg": "Successfully added " + ipport + " to view"})
	} else {
		log.Println(myIPport(), ":", ipport, "is already in view")
		sendRespondMsg(w, http.StatusNotFound, &map[string]interface{}{"result": "Error", "msg": ipport + " is already in view"})
	}
	broadcastCustom("view", "PUT", form, globalBroadcast, responseNotRequired)
}

func deleteView(w http.ResponseWriter, r *http.Request) {
	form := parseForm(r)
	ipPort := form["ip_port"]
	log.Println("REACHED DELETE VIEW*****")
	broadcastCustom("view", "DELETE", form, globalBroadcast, responseNotRequired)
	if globalView.deleteNode(ipPort) {
		log.Println(myIPport(), ": after delete view:", globalView.localView)
		sendRespondMsg(w, http.StatusOK, &map[string]interface{}{"result": "Success", "msg": "Successfully removed " + ipPort + " from view"})

	} else {
		sendRespondMsg(w, http.StatusNotFound, &map[string]interface{}{"result": "Error", "msg": ipPort + " is not in current view"})
	}
}

func receiveKVS(w http.ResponseWriter, r *http.Request) {
	if k.isEmpty() {
		json.NewDecoder(r.Body).Decode(&k.Dict)
		log.Println(myIPport(), ": KVS updated")
	}
}

func sendKVS(w http.ResponseWriter, ipport string) {
	log.Println(myIPport()+": Sent kvs to:", ipport)
	encodedKVS, _ := json.Marshal(k.Dict)
	url := "http://" + ipport + "/view"
	client := http.Client{}

	request, err := http.NewRequest("POST", url, bytes.NewBuffer(encodedKVS))
	request.Header.Set("Content-Type", "application/json")
	if err != nil {
		log.Println(err)
	}
	_, err = client.Do(request)
	if err != nil {
		log.Println(err)
	}

}
