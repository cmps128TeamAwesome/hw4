package main

import (
	"log"
	"strconv"
	"strings"
)

type globalViewType struct {
	localView   [][]string
	shardID     int
	numOfShards int
	numOfNodes  int
}

func newGloballView(viewLocal string, numOfShards int) *globalViewType {
	g := new(globalViewType)
	log.Println("numOfShards:", g.numOfShards)
	g.numOfShards = numOfShards
	g.localView = make([][]string, g.numOfShards)

	if viewLocal != "" {
		viewArray := strings.Split(viewLocal, ",")
		if g.numOfShards == 0 {
			g.numOfShards = 1
		}
		for shard := range g.localView {
			g.localView[shard] = make([]string, 0)
		}
		for _, ipPort := range viewArray {
			shardID := getID(ipPort) % g.numOfShards
			g.localView[shardID] = append(g.localView[shardID], ipPort)
			g.numOfNodes++
		}
	}
	g.renewMyShardID()
	return g
}

func (g *globalViewType) size() int {
	return len(g.localView)
}

func (g *globalViewType) localSize(l int) int {
	return len(g.localView[l])
}

func (g *globalViewType) getLocalView(index int) []string {
	return g.localView[index]
}

func (g *globalViewType) toString(shardID int) string {
	str := ""
	if shardID == -1 {
		for shardID := range g.localView {
			str += strconv.Itoa(shardID) + ","
		}
		str = strings.TrimRight(str, ",")
	} else {
		str = strings.Join(g.getLocalView(shardID), ",")
		str = strings.TrimLeft(str, ",")
	}
	return str
}

func (g *globalViewType) addNode(targetIP string) bool {
	shardID := hashIP(targetIP)
	for _, node := range g.localView[shardID] {
		if node == targetIP {
			return false
		}
	}
	g.numOfNodes++
	g.localView[shardID] = append(g.localView[shardID], targetIP)
	return true
}

func (g *globalViewType) deleteNode(targetIP string) bool {
	shardID := hashIP(targetIP)
	for i, node := range g.getLocalView(shardID) {
		if node == targetIP {
			g.numOfNodes--
			lastElmIndex := g.localSize(shardID) - 1
			g.localView[shardID][i] = g.getLocalView(shardID)[lastElmIndex]
			g.localView[shardID] = g.getLocalView(shardID)[:lastElmIndex]
			if targetIP == myIPport() {
				log.Println(myIPport(), ": Oh, Oh. It is me. Deleting myself. GoodBye!!!")
				g.localView = g.localView[:0]
				return true
			}
			if g.localSize(shardID) < 2 && g.numOfShards != 1 {
				g.numOfShards--
				g.repartitionView()
			}
			return true
		}
	}
	return false
}

func myIPport() string {
	return iPPortPrivate
}

func (g *globalViewType) myShardID() int {
	return g.shardID
}

func (g *globalViewType) decNumOfShards() {
	g.numOfShards--
}

func (g *globalViewType) incNumOfShards() {
	g.numOfShards++
}

func (g *globalViewType) getNumOfShards() int {
	return g.numOfShards
}

func (g *globalViewType) renewMyShardID() {
	id := getID(myIPport())
	g.shardID = id % g.numOfShards
}

func (g *globalViewType) repartitionView() {
	var view string
	for lv := range globalView.localView {
		view += globalView.toString(lv) + ","
	}
	view = strings.TrimRight(view, ",")
	log.Println("View changed to this", view)
	globalView = newGloballView(view, g.numOfShards)
	k.rehashAllKeys()
}
