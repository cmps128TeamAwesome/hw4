package main

import (
	"log"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

func getMyShardID(w http.ResponseWriter, r *http.Request) {
	sendRespondMsg(w, http.StatusOK, &map[string]interface{}{"id": globalView.myShardID()})
}

func getAllShardID(w http.ResponseWriter, r *http.Request) {
	sendRespondMsg(w, http.StatusOK, &map[string]interface{}{"result": "Success", "shard_ids": globalView.toString(-1)})
}

func getMembersOfShardID(w http.ResponseWriter, r *http.Request) {
	shardIDString := mux.Vars(r)["shard_id"]
	shardID, err := strconv.Atoi(shardIDString)
	if err != nil {
		log.Println("Shard id must be an integer")
	} else if shardID > globalView.size()-1 {
		sendRespondMsg(w, http.StatusNotFound, &map[string]interface{}{"result": "Error", "msg": "No shard with id " + strconv.Itoa(shardID)})
	} else {
		log.Println("Sending this:", globalView.toString(shardID))
		sendRespondMsg(w, http.StatusOK, &map[string]interface{}{"result": "Success", "members": globalView.toString(shardID)})
	}

}

func getCountOfShardIDKvs(w http.ResponseWriter, r *http.Request) {
	shardIDString := mux.Vars(r)["shard_id"]
	shardID, err := strconv.Atoi(shardIDString)
	if err != nil {
		log.Println("Shard id must be an integer")
	} else if shardID > globalView.numOfShards {
		sendRespondMsg(w, http.StatusNotFound, &map[string]interface{}{"result": "Error", "msg": "No shard with id " + strconv.Itoa(shardID)})
	} else if shardID == globalView.myShardID() {
		log.Println("My KVS has:", len(k.Dict))
		sendRespondMsg(w, http.StatusOK, &map[string]interface{}{"result": "Success", "Count": len(k.Dict)})
	} else {
		resp, statusCode := broadcastCustom("shard/count/"+shardIDString, "GET", map[string]string{}, shardID, responseRequired)
		sendRespondMsg(w, statusCode, &resp)
	}
}

func changeShardNumber(w http.ResponseWriter, r *http.Request) {
	form := parseForm(r)
	possibleShard, _ := strconv.Atoi(form["num"])
	minNumShards := globalView.numOfNodes / possibleShard
	if possibleShard > globalView.numOfNodes {
		sendRespondMsg(w, http.StatusBadRequest, &map[string]interface{}{"result": "Error", "msg": "Not enough nodes for " + form["num"] + " shards"})
	} else if minNumShards < 2 {
		sendRespondMsg(w, http.StatusBadRequest, &map[string]interface{}{"result": "Error", "msg": "Not enough nodes. " + form["num"] + " shards result in a nonfault tolerant shard"})
	} else {
		if possibleShard != globalView.numOfShards {
			globalView.numOfShards = possibleShard
			globalView.repartitionView()
			log.Println(globalView.localView)
		}
		broadcastCustom("shard/changeShardNumber", "PUT", form, globalBroadcast, responseNotRequired)
		sendRespondMsg(w, http.StatusOK, &map[string]interface{}{"result": "Success", "shard_ids": globalView.toString(-1)})
	}
}
