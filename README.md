# A single-site Key-value store with forwarding

# Motivation
To explore parallelization with fault tolerant replication 

# Implementation
Partition nodes into shards, replicate data within each shard

# Installation
Make sure you have Docker installed

# Testing

Make sure to modify these variables if needed in `docker_control.go`:
- var subnetName = YOURSUBNETNAME (e.g. "mynet")
- var prefixSubnetAddress = YOUR SUBNET PREFIX (e.g. "10.0.0.")
- var prefixPort = YOUR PREFIX PORT (e.g. "80")
- var path = YOUR PATH TO SOURCE CODE (e.g. "../src/.")

To build and run, do:
```
go test hw4_test.go docker_control.go -v -build
```
- removes image 'Testing'
- builds image 'Testing'
- removes all containers
- begins the test

To test only, do: 
```
go test hw4_test.go docker_control.go -v
```
- removes all containers
- begins the test

To run specific test, do:
```
go test hw4_test.go docker_control.go -v -run <TESTNAME>
```
<TESTNAME> can be the following: 
- S, SH, SHARD, or ShardIDIsUniqueFromEverynode
- Golang searches through `hw4_test.go` for tests with the <TESTNAME> and runs all of those tests. 


# How can you contribute?
- Feel free to clone and write more tests to this
- No need to rebuild the image to run new tests. 


# Contributors
- Akobir - akhamido@ucsc.edu
- Vien Van - vhvan@ucsc.edu
- Rob Phillips - rogphill@ucsc.edu
- Tarun Salh - tsalh@ucsc.edu

# Purpose
HW4 for CMPS128 - Distributed Systems Fall 2018 @ UCSC
